L.Control.Tabmenu = L.Control.extend({
	includes: L.Evented.prototype,
	options: {
		container: null,
		position: 'right',
		className: '',
		autoPan: false,
		width: 400,
		tabSize: 60,
	},
	onAdd: function (map) {
		if (this.options.container) {
			_container = typeof this.options.container === 'string' ? L.DomUtil.get(this.options.container) : this.options.container;
			this._tabsUtil = _container.getElementsByClassName('leaflet-tabmenu-tabs')[0];
			this._panelsUtil = _container.getElementsByClassName('leaflet-tabmenu-panels')[0];
			this._addPanelsFromDOM();
		} else {
			_container = L.DomUtil.create('div', `leaflet-tabmenu ${this.options.className} collapsed`);
			this._tabsUtil = L.DomUtil.create('ul', 'leaflet-tabmenu-tabs', _container);
			this._panelsUtil = L.DomUtil.create('div', 'leaflet-tabmenu-panels', _container);
		}

		this.setTabSize(this._tabSize);
		this.setWidth(this._width);

		return _container;
	},
	addTo: function (map) {
		this._tabs = [];
		this._panels = [];
		this._map = map;
		this._autoPan = this.options.autoPan;
		this._width = this.options.width;
		this._tabSize = this.options.tabSize;

		this._container = this.onAdd(map);

		L.DomUtil.addClass(this._container, 'leaflet-control');
		L.DomUtil.addClass(this._container, 'leaflet-tabmenu-' + this.getPosition());
		L.DomEvent.disableScrollPropagation(this._container);
		L.DomEvent.disableClickPropagation(this._container);
		L.DomEvent.on(this._container, 'contextmenu', L.DomEvent.stopPropagation);
		if (L.Browser.touch) {
			L.DomUtil.addClass(this._container, 'leaflet-touch');
		}

		map._container.insertBefore(this._container, map._container.firstChild);

		return this;
	},
	onRemove: function (map) {
		for (let tab of this._tabs) {
			this._tabClickEvent(tab, 'off');
		}

		this._tabs = [];
		this._panels = [];

		return this;
	},
	add: function (data) {
		var tabUtil, aUtil, iconUtil, panelUtil, titleUtil, className;

		className = data.className || '';
		tabUtil = L.DomUtil.create('li', `leaflet-tabmenu-tab ${className}`, this._tabsUtil);
		aUtil = L.DomUtil.create('a', null, tabUtil);
		iconUtil = L.DomUtil.create('i', data.icon, aUtil);
		titleUtil = L.DomUtil.create('span', null, aUtil);
		panelUtil = L.DomUtil.create('div', `leaflet-tabmenu-panel ${className}`, this._panelsUtil);

		if (typeof data.content === 'string') {
			panelUtil.innerHTML = data.content;
		} else if (data.content) {
			panelUtil.append(data.content);
		}

		aUtil.href = data.href || `#${data.id}`;
		titleUtil.textContent = data.title;
		panelUtil.id = data.id;
		panelUtil._tabmenu = this;
		tabUtil._id = data.id;
		tabUtil._tabmenu = this;

		if (data.href) {
			L.DomUtil.addClass(tabUtil, 'link');
			aUtil.target = '_blank';
		}
		if (data.disabled) {
			L.DomUtil.addClass(tabUtil, 'disabled');
		}

		this._tabs.push(tabUtil);
		this._panels.push(panelUtil);

		if (data.hidden) {
			this.hide(data.id);
		}
		this._tabClickEvent(tabUtil, 'on');

		return this;
	},
	remove: function (id) {
		var tab = this._getTab(id);
		if (!tab) return this;
	},
	onTabClick: function (event) {
		if (L.DomUtil.hasClass(this, 'active')) {
			this._tabmenu.close();
		} else {
			this._tabmenu.open(this.querySelector('a').hash.slice(1));
		}
	},
	open: function (id) {
		var tab = this._getTab(id);
		if (!tab) return this;

		if (L.DomUtil.hasClass(tab, 'disabled')) {
			return this;
		}

		for (let panel of this._panels) {
			if (panel.id === id) {
				L.DomUtil.addClass(panel, 'active');
			} else if (L.DomUtil.hasClass(panel, 'active')) {
				L.DomUtil.removeClass(panel, 'active');
			}
		}

		for (let tab of this._tabs) {
			if (tab.querySelector('a').hash === '#' + id) {
				L.DomUtil.addClass(tab, 'active');
			} else if (L.DomUtil.hasClass(tab, 'active')) {
				L.DomUtil.removeClass(tab, 'active');
			}
		}

		if (L.DomUtil.hasClass(this._container, 'collapsed')) {
			L.DomUtil.removeClass(this._container, 'collapsed');
			this._pan(true);
		}

		this.fire('open', { id: id });

		return this;
	},
	close: function () {
		for (let tab of this._tabs) {
			if (L.DomUtil.hasClass(tab, 'active')) {
				L.DomUtil.removeClass(tab, 'active');
			}
		}

		if (!L.DomUtil.hasClass(this._container, 'collapsed')) {
			L.DomUtil.addClass(this._container, 'collapsed');
			this._pan(false);
		}

		this.fire('close');

		return this;
	},
	disable: function (id) {
		var tab = this._getTab(id);
		if (!tab) return this;

		if (!L.DomUtil.hasClass(tab, 'disabled')) {
			L.DomUtil.addClass(tab, 'disabled');
		}

		return this;
	},
	enable: function (id) {
		var tab = this._getTab(id);
		if (!tab) return this;

		L.DomUtil.removeClass(tab, 'disabled');

		return this;
	},
	hide: function (id) {
		var tab = this._getTab(id);
		if (!tab) return this;

		if (!L.DomUtil.hasClass(tab, 'hidden')) {
			L.DomUtil.addClass(tab, 'hidden');
		}

		return this;
	},
	show: function (id) {
		var tab = this._getTab(id);
		if (!tab) return this;

		L.DomUtil.removeClass(tab, 'hidden');

		return this;
	},
	setAutoPan: function (autoPan) {
		this._autoPan = autoPan;
	},
	getAutoPan: function () {
		return this._autoPan;
	},
	setWidth: function (width) {
		if (isNaN(width)) return;

		this._width = width + 'px';
		document.documentElement.style.setProperty('--tabmenu-width', this._width);
	},
	setTabSize: function (size) {
		if (isNaN(size)) return;

		this._tabSize = size + 'px';
		document.documentElement.style.setProperty('--tab-size', this._tabSize);
	},
	_addPanelsFromDOM: function () {
		Array.from(this._tabsUtil.children).forEach((tab) => {
			if (L.DomUtil.hasClass(tab, 'leaflet-tabmenu-tab')) {
				tab._tabmenu = this;
				tab._id = tab.getElementsByTagName('a')[0].hash.slice(1);
				this._tabClickEvent(tab, 'on');
				this._tabs.push(tab);
			}
		});

		Array.from(this._panelsUtil.children).forEach((panel) => {
			if (L.DomUtil.hasClass(panel, 'leaflet-tabmenu-panel')) {
				panel._tabmenu = this;
				this._panels.push(panel);
			}
		});
	},
	_tabClickEvent: function (tab, handler) {
		var link = tab.querySelector('a');

		if (handler === 'on') {
			if (link.getAttribute('href')[0] !== '#') {
			} else {
				L.DomEvent.on(tab.querySelector('a'), 'click', L.DomEvent.preventDefault, tab);
				L.DomEvent.on(tab.querySelector('a'), 'click', this.onTabClick, tab);
			}
		} else {
			L.DomEvent.off(tab.querySelector('a'), 'click', this.onTabClick, tab);
		}
	},
	_getTab: function (id) {
		for (let tab of this._tabs) {
			if (tab._id === id) {
				return tab;
			}
		}

		this._error(`tab with the id ${id} not found`);
		return null;
	},
	_pan: function (open) {
		if (!this.getAutoPan()) {
			return;
		}
		var _panWidth = 0;

		if (open) {
			this._webkitLogicalWidth = Number.parseInt(L.DomUtil.getStyle(this._panelsUtil, 'webkitLogicalWidth'));
		}
		_panWidth = this._webkitLogicalWidth;

		if ((open && this.options.position === 'left') || (!open && this.options.position === 'right')) {
			_panWidth *= -1;
		}

		this._map.panBy([_panWidth, 0], { duration: 0.25 });
	},
	_error: function (text) {
		console.error(`Leaflet.TabMenu: ${text}`);
	},
});

L.control.tabmenu = function (options) {
	return new L.Control.Tabmenu(options);
};
