# Leaflet.Tabmenu [Demo](http://leaflet-tabmenu.manuel-ri.de/)

A leaflet plugin to add a multi tab menu inside the map.
![Image](https://gitlab.com/manuel.richter95/leaflet.tabmenu/-/raw/master/doc/leaflet-tabmenu.PNG)

## Installation

```bash
npm install leaflet-tabmenu
```

## Usage

You can choose to use HTML and/or JavaScript to create the TabMenu or add tabs and panels

```html
<div id="mapid"></div>
<div id="tabmenu" class="leaflet-tabmenu collapsed">
	<!--Tabs-->
	<ul class="leaflet-tabmenu-tabs">
		<li class="leaflet-tabmenu-tab">
			<a href="#settings">
				<i class="fa fa-cog"></i>
				<span>Settings</span>
			</a>
		</li>
	</ul>

	<!--Panels-->
	<div class="leaflet-tabmenu-panels">
		<div class="leaflet-tabmenu-panel" id="settings">
			<h2>Lorem ipsum</h2>
			Lorem ipsum dolor sit amet, consetetur sadipscing elitr
		</div>
	</div>
</div>

<!--Some HTML content-->
<div id="somePanel">This is a User panel</div>
```

```javascript
var mymap = L.map('mapid').setView([0, 0], 3);
L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(mymap);
var tabMenu = L.control
	.tabmenu({
		position: 'right', //left or right
		container: 'tabmenu', //if no container given it will be created
		autoPan: true, //centers the map on menu open/close,
		width: 400, //menu panel width in px
		tabSize: 60, //menu tabs height/width in px
	})
	.addTo(mymap);

//add custom HTML content
tabMenu.add({
	id: 'home',
	icon: 'fa fa-home',
	title: 'Home',
	content: `
           <div>
                A tabmenu for leaflet
           </div>
       `,
});
//get a HTML element and pass it as content
tabMenu.add({
	id: 'user',
	icon: 'fa fa-user',
	title: 'User',
	className: 'someClass',
	content: document.getElementById('somePanel'),
});
//use a tabmenu button as a link
tabMenu.add({
	id: 'google',
	href: 'https://www.google.de',
	icon: 'fa fa-google',
});
```

###Methods

```javascript
tabMenu.open('home'); //open the "home" panel
tabMenu.close(); //close menu
tabMenu.disable('home'); //disable the "home" panel tab
tabMenu.enable('home'); //disable the "home" panel tab
tabMenu.hide('home'); //hide the "home" panel tab
tabMenu.show('home'); //show the "home" panel tab
```

###Events

```javascript
tabMenu.on('open', (event) => {
	console.log(event.id);
}); // "open" event that will be fired when a panel has been opened
tabMenu.on('close', (event) => {}); // "close" event that will be fired when a panel has been opened
```

### TabMenu Control options

| Property  | Type               | Default | Description                                                  |
| --------- | ------------------ | ------- | ------------------------------------------------------------ |
| container | String/HTMLElement | null    | The TabMenus container                                       |
| position  | String             | 'right' | Position of the TabMenu. Possible values are _right_, _left_ |
| autoPan   | Boolean            | false   | Centers the map on opening/closing the menu                  |
| width     | Number             | 400     | Width of the menu panels in px                               |
| tabSize   | Number             | 60      | Width and Height of the menu tabs in px                      |
| className | String             | null    | A custom class name                                          |

## TabMenu Contol Methods

| Method            | Returns | Description                                 |
| ----------------- | ------- | ------------------------------------------- |
| add(options)      | this    | add a menu panel                            |
| remove(id)        | this    | remove a menu panel                         |
| open(id)          | this    | open a menu panel                           |
| close()           | this    | close/collapse the opened menu panel        |
| disable(id)       | this    | disable a menu tab button                   |
| enable(id)        | this    | enable a disabled menu tab button           |
| hide(id)          | this    | hide a menu tab button                      |
| show(id)          | this    | show a hidden menu tab button               |
| setAutoPan(value) | void    | set autoPan                                 |
| setWidth(width)   | void    | set the TabMenu width                       |
| setTabSize(size)  | void    | set the TabMenu tab size (height and width) |

### TabMenu add(options) options

| Property  | Type               | Default | Description                      |
| --------- | ------------------ | ------- | -------------------------------- |
| id        | String             | null    | The tab/panel ID                 |
| icon      | String             | null    | icon of the menu tab             |
| title     | String             | null    | Title of the menu tab            |
| href      | String             | null    | href to a location               |
| content   | String/HTMLElement | null    | Content of the menu panel        |
| disabled  | Boolean            | false   | If true the tab will be disabled |
| hidden    | Boolean            | false   | If true the tab won't be visible |
| className | String             | null    | A custom class name              |

Got inspired by https://github.com/turbo87/sidebar-v2/ and used the project as a reference, so a thanks goes to this author!
